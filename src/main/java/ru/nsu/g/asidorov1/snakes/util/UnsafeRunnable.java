package ru.nsu.g.asidorov1.snakes.util;

@FunctionalInterface
public interface UnsafeRunnable {

    void run() throws Exception;
}
