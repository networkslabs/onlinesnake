package ru.nsu.g.asidorov1.snakes;

import lombok.experimental.UtilityClass;
import lombok.val;
import ru.nsu.g.asidorov1.snakes.descriptors.config.Config;
import ru.nsu.g.asidorov1.snakes.descriptors.config.InvalidConfigException;
import ru.nsu.g.asidorov1.snakes.event.EventProcessor;
import ru.nsu.g.asidorov1.snakes.event.events.*;
import ru.nsu.g.asidorov1.snakes.gui.menu.MenuWindow;
import ru.nsu.g.asidorov1.snakes.gui.util.GuiUtils;
import ru.nsu.g.asidorov1.snakes.message.AddressedMessage;
import ru.nsu.g.asidorov1.snakes.net.MessageHistory;
import ru.nsu.g.asidorov1.snakes.net.MessageReceiver;
import ru.nsu.g.asidorov1.snakes.net.MessageSender;
import ru.nsu.g.asidorov1.snakes.net.TimeoutWatcher;
import ru.nsu.g.asidorov1.snakes.util.Constants;
import ru.nsu.g.asidorov1.snakes.util.Scheduler;
import ru.nsu.g.asidorov1.snakes.util.TimerWithLogger;
import ru.nsu.g.asidorov1.snakes.util.UnsafeRunnable;

import java.io.IOException;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;


@UtilityClass
public class Application {

    private final
    String LOGGING_PROPERTIES_FILE = "/logging.properties";
    private final Logger logger;

    static {
        tryInitLogger();
        logger = Logger.getLogger(Application.class.getSimpleName());
    }

    public void main(final String[] args) {
        GuiUtils.setUIComponentsColors();

        var cfg = Config.DEFAULT_CONFIG;
        try {
            cfg = Config.load();
        } catch (final InvalidConfigException e) {
            logger.warning("Invalid config: " + e.getMessage() + ", will use default instead");
        }
        val config = cfg;

        val eventProcessor = new EventProcessor();
        val eventProcessorThread = createEventProcessorDaemon(eventProcessor);

        val messageHistory = new MessageHistory();

        eventProcessor.addHandler(
                event -> event instanceof IncomingMessage
                        && event.<IncomingMessage>get().message.getMessage().hasAck(),
                event -> messageHistory.deliveryConfirmed(event.<IncomingMessage>get().message));

        val announcements = new HashSet<AddressedMessage>();

        try (
                val multicastReceiverSocket = new MulticastSocket(Constants.MULTICAST_PORT);
                val generalPurposeSocket = new MulticastSocket()) {
            setGivenOrAnyInterface(multicastReceiverSocket, generalPurposeSocket, "Ethernet");

            logger.info("Running on " + generalPurposeSocket.getLocalSocketAddress());

            val sender = new MessageSender(generalPurposeSocket, messageHistory);

            // General handlers
            eventProcessor.addHandler(
                    event -> event instanceof OutgoingMessage,
                    event -> sender.send(event.<OutgoingMessage>get().message));

            eventProcessor.addHandler(
                    event -> event instanceof NotAcknowledged,
                    event -> eventProcessor.submit(new OutgoingMessage(event.<NotAcknowledged>get().message)));

            eventProcessor.addHandler(
                    event -> event instanceof NodeTimedOut,
                    event -> messageHistory.removeConnectionRecord(event.<NodeTimedOut>get().nodeAddress));
            // /General handlers

            val unicastReceiver = new MessageReceiver(generalPurposeSocket, message -> {
                messageHistory.messageReceived(message.getAddress());
                eventProcessor.submit(new IncomingMessage(message));
            });

            val unicastReceiverThread = createUnicastReceiverDaemon(unicastReceiver);

            val multicastReceiver = new MessageReceiver(multicastReceiverSocket, message -> {
                messageHistory.announcementReceived(message.getAddress());
                eventProcessor.submit(new Announcement(message));
            });

            val timer = new TimerWithLogger();
            val scheduler = Scheduler.fromTimer(timer);

            val timeoutManager = new TimeoutWatcher(
                    eventProcessor, messageHistory, config.getPingDelayMs(),
                    config.getPingDelayMs(), config.getNodeTimeoutMs(),
                    Constants.ANNOUNCE_DELAY_MS * 3 / 2);
            val handleTimeoutsTask = scheduler.schedule(
                    timeoutManager::handleTimeouts, (config.getPingDelayMs() + 1) / 2);


            unicastReceiverThread.start();
            eventProcessorThread.start();

            val menuWindow = new MenuWindow(
                    "Snakes", config, announcements,
                    (name, baseConfig, view) ->
                            Node.createHost(name, baseConfig, view, scheduler, eventProcessor),
                    (name, baseConfig, host, view, onSuccess, onError) -> {
                        try {
                            Node.createClient(
                                    name, baseConfig, host, view, scheduler, eventProcessor, sender::setMasterAddressSupplier, onSuccess, onError);
                        } catch (final InterruptedException unused) {
                            logger.info("Interrupted when connecting to " + host);
                        }
                    });

            menuWindow.getExitHookRegisterer().accept(handleTimeoutsTask::cancel);
            menuWindow.getExitHookRegisterer().accept(timer::cancel);
            menuWindow.getExitHookRegisterer().accept(generalPurposeSocket::close);
            menuWindow.getExitHookRegisterer().accept(multicastReceiverSocket::close);
            menuWindow.makeVisible();

            val runningGamesView = menuWindow.getRunningGamesView();

            eventProcessor.addHandler(
                    event -> event instanceof Announcement,
                    event -> {
                        synchronized (announcements) {
                            val message = event.<Announcement>get().message;
                            announcements.removeIf(it -> it.getAddress().equals(message.getAddress()));
                            announcements.add(message);
                        }
                        runningGamesView.updateView();
                    });
            eventProcessor.addHandler(
                    event -> event instanceof AnnouncementTimedOut,
                    event -> {
                        val fromAddress = event.<AnnouncementTimedOut>get().fromAddress;
                        synchronized (announcements) {
                            announcements.removeIf(it -> it.getAddress().equals(fromAddress));
                        }
                        messageHistory.removeAnnouncementRecord(fromAddress);
                        runningGamesView.updateView();
                    });

            multicastReceiver.run();
        } catch (final InterruptedException e) {
            logger.info(Thread.currentThread().getName() + " interrupted");
        } catch (final Exception e) {
            logger.severe(e.getMessage());
        }
    }

    private void tryInitLogger() {
        try {
            final var config = Application.class.getResourceAsStream(LOGGING_PROPERTIES_FILE);
            if (config == null) {
                throw new IOException("Cannot load \"" + LOGGING_PROPERTIES_FILE + "\"");
            }
            LogManager.getLogManager().readConfiguration(config);
        } catch (final IOException e) {
            System.err.println("Could not setup logger configuration: " + e.getMessage());
        }
    }

    private Thread createUnicastReceiverDaemon(
            final UnsafeRunnable task) {
        val thread = new Thread(() -> {
            try {
                task.run();
            } catch (final Exception e) {
                logger.info(Thread.currentThread().getName() + ": " + e.getMessage());
            }
        });
        thread.setName("Unicast-Receiver-Thread");
        thread.setDaemon(true);
        return thread;
    }

    private Thread createEventProcessorDaemon(
            final Runnable task) {
        val thread = new Thread(task);
        thread.setName("Event-Processor-Thread");
        thread.setDaemon(true);
        return thread;
    }

    private void setGivenOrAnyInterface(
            final MulticastSocket multicastReceiver,
            final MulticastSocket generalPurposeSocket,
            final String interfaceName) throws IOException {
        if (interfaceName != null) {
            val givenInterface = NetworkInterface.getByName(interfaceName);
            if (givenInterface != null) {
                multicastReceiver.joinGroup(Constants.announceAddress, givenInterface);
                generalPurposeSocket.setNetworkInterface(givenInterface);
                return;
            }
        }

        val availableInterfaces = getAvailableInterfaces();
        for (final var it : availableInterfaces) {
            try {
                multicastReceiver.joinGroup(Constants.announceAddress, it);
                generalPurposeSocket.setNetworkInterface(it);
                logger.info("Using network interface interface " + it.getName());
                return;
            } catch (final IOException e) {
                logger.info("Cannot use network interface " + it.getName());
            }
        }
        throw new IOException("Cannot use any network interface");
    }

    private Collection<NetworkInterface> getAvailableInterfaces() throws IOException {
        try {
            return NetworkInterface.networkInterfaces()
                    .filter(it -> {
                        try {
                            return it.isUp()
                                    && it.supportsMulticast()
                                    && !it.isLoopback()
                                    && it.inetAddresses()
                                    .anyMatch(
                                            address -> address.getClass()
                                                    == Constants.announceAddress.getAddress().getClass());
                        } catch (final SocketException e) {
                            return false;
                        }
                    })
                    .collect(Collectors.toSet());
        } catch (final SocketException e) {
            logger.severe("Cannot to obtain information about available interfaces");
            throw e;
        }
    }
}
