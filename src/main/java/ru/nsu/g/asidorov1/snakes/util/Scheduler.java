package ru.nsu.g.asidorov1.snakes.util;

import lombok.experimental.UtilityClass;
import lombok.val;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

@FunctionalInterface
public interface Scheduler {

    TimerTask schedule(
            final UnsafeRunnable executable,
            final int periodMs);

    static Scheduler fromTimer(final Timer timer) {
        return (executable, period) -> {
            SchedulerLoggerWrapper.logger.info("Task scheduled");
            val task = new TimerTask() {

                @Override
                public void run() {
                    try {
                        executable.run();
                    } catch (final Exception e) {
                        SchedulerLoggerWrapper.logger.warning(
                                "Executable thrown an exception " + e.getClass().getSimpleName()
                                        + ": " + e.getMessage());
                        e.printStackTrace();
                        timer.cancel();
                    }
                }

                @Override
                public boolean cancel() {
                    SchedulerLoggerWrapper.logger.info("Task cancelled");
                    return super.cancel();
                }
            };
            timer.scheduleAtFixedRate(task, period, period);
            return task;
        };
    }
}

@UtilityClass
class SchedulerLoggerWrapper {

    final Logger logger = Logger.getLogger(Scheduler.class.getSimpleName());
}
