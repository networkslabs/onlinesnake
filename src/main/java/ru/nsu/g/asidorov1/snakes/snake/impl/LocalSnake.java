package ru.nsu.g.asidorov1.snakes.snake.impl;

import lombok.val;
import ru.nsu.g.asidorov1.snakes.plane.BoundedMovablePoint;
import ru.nsu.g.asidorov1.snakes.plane.BoundedPoint;
import ru.nsu.g.asidorov1.snakes.plane.Direction;
import ru.nsu.g.asidorov1.snakes.snake.Snake;
import ru.nsu.g.asidorov1.snakes.snake.SnakeInfo;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.function.Consumer;
import java.util.function.Function;

public class LocalSnake implements Snake {

    private final Deque<BoundedMovablePoint> segments = new ArrayDeque<>();
    private
    Direction direction;
    private Direction nextDirection;
    private boolean alive = true;
    private final int playerId;

    public LocalSnake(
            final int playerId,
            final BoundedPoint headPosition,
            final Direction direction,
            final int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Invalid size");
        }

        this.playerId = playerId;

        val head = new BoundedMovablePoint(headPosition);
        this.segments.add(head);
        for (int i = 1; i < size; i += 1) {
            this.segments.add(this.segments.getLast().moved(direction.getOpposite()));
        }

        this.direction = direction;
        this.nextDirection = direction;
    }

    public static LocalSnake copyOf(final SnakeInfo other) {
        return new LocalSnake(other);
    }

    protected LocalSnake(final SnakeInfo other) {
        this.playerId = other.getPlayerId();
        other.forEachSegment(point -> this.segments.add(new BoundedMovablePoint(point)));
        if (other.isZombie()) {
            this.zombify();
        }
        this.direction = other.getDirection();
        this.nextDirection = this.direction;
    }

    @Override
    public void move(final Function<BoundedPoint, Boolean> isFood) {
        val head = this.segments.peekFirst();
        if (head == null) {
            throw new IllegalStateException("Headless");
        }
        this.segments.push(head.moved(this.nextDirection));
        if (!isFood.apply(this.getHead())) {
            this.segments.removeLast();
        }
        this.direction = this.nextDirection;
    }

    @Override
    public void zombify() {
        this.alive = false;
    }

    @Override
    public void changeDirection(
            final Direction direction) {
        if (this.direction.isNotOppositeTo(direction)) {
            this.nextDirection = direction;
        }
    }

    @Override
    public BoundedPoint getHead() {
        val head = this.segments.peekFirst();
        if (head == null) {
            throw new IllegalStateException("Headless");
        }
        return head;
    }

    @Override
    public void forEachSegment(
            final Consumer<BoundedPoint> action) {
        this.segments.forEach(action);
    }

    @Override
    public int getPlayerId() {
        return this.playerId;
    }

    @Override
    public Direction getDirection() {
        return this.direction;
    }

    @Override
    public boolean isZombie() {
        return !this.alive;
    }
}
