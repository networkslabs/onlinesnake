package ru.nsu.g.asidorov1.snakes.descriptors.config;

public interface NetworkConfig {

    int getPingDelayMs();

    int getNodeTimeoutMs();
}
