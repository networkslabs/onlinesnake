package ru.nsu.g.asidorov1.snakes.net;

import com.google.protobuf.InvalidProtocolBufferException;
import lombok.RequiredArgsConstructor;
import lombok.val;
import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.g.asidorov1.snakes.message.AddressedMessage;
import ru.nsu.g.asidorov1.snakes.util.Constants;
import ru.nsu.g.asidorov1.snakes.util.UnsafeConsumer;
import ru.nsu.g.asidorov1.snakes.util.UnsafeRunnable;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Arrays;
import java.util.logging.Logger;

@RequiredArgsConstructor
public class MessageReceiver implements UnsafeRunnable {

    private static final Logger logger = Logger.getLogger(MessageReceiver.class.getSimpleName());

    private final DatagramSocket in;
    private final
    UnsafeConsumer<AddressedMessage> onReceived;

    private final byte[] buffer = new byte[Constants.MAX_PACKET_SIZE_B];

    @Override
    public void run() throws Exception {
        while (true) {
            val packet = new DatagramPacket(this.buffer, 0, this.buffer.length);

            try {
                this.in.receive(packet);
                logger.finest(" Received " + packet.getLength() + " bytes long packet from "
                        + packet.getSocketAddress());
                if (!(packet.getSocketAddress() instanceof InetSocketAddress)) {
                    logger.info(" Unsupported remote socket address: "
                            + packet.getSocketAddress().getClass().getName());
                    continue;
                }
                if (this.in.getLocalPort() == packet.getPort() && Utility.isThisMyIpAddress(packet.getAddress())) {
                    logger.info("Received a packet from self, dropping");
                    continue;
                }
                val fromAddress = (InetSocketAddress) packet.getSocketAddress();

                val data = Arrays.copyOfRange(packet.getData(), 0, packet.getLength());
                try {
                    val contents = SnakesProto.GameMessage.parseFrom(data);
                    this.onReceived.accept(AddressedMessage.create(fromAddress, contents));
                } catch (final InvalidProtocolBufferException e) {
                    logger.info("Received invalid message: " + e.getMessage());
                }
            } catch (final SocketException e) {
                logger.info("SocketException: " + e.getMessage());
                return;
            }
        }
    }
}
