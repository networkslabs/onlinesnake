package ru.nsu.g.asidorov1.snakes.descriptors.player;

import lombok.*;
import me.ippolitov.fit.snakes.SnakesProto;

@AllArgsConstructor
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
public class PlayerDescriptor implements PlayerInfo {
    private final String name;
    @EqualsAndHashCode.Include
    private final int id;
    @Setter
    private String address;
    @Setter
    private int port;
    private SnakesProto.NodeRole role;
    private final SnakesProto.PlayerType type;
    private int score = 0;

    public static PlayerDescriptor copyOf(final PlayerInfo other) {
        return new PlayerDescriptor(other);
    }

    private PlayerDescriptor(final PlayerInfo other) {
        this(
                other.getName(), other.getId(), other.getAddress(),
                other.getPort(), other.getRole(), other.getType(), other.getScore());
    }

    public void setRole(final SnakesProto.NodeRole newRole) {
        this.role = newRole;
    }

    public void increaseScore() {
        this.score += 1;
    }
}
