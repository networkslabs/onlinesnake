package ru.nsu.g.asidorov1.snakes.gui.game;

import ru.nsu.g.asidorov1.snakes.game.GameModel;

import java.awt.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface SnakesGameView {

    void makeVisible();

    /**
     * Render any changes happened to the model this view is bound to
     *
     * @throws IllegalStateException if not bound to any model
     */
    void updateView() throws IllegalStateException;

    /**
     * Bind this view to a model
     *
     * @param gameState game model to bind to
     */
    void bindTo(final GameModel gameState);

    void setPreferredColour(final int playerId, final Color color);

    Color getColour(final int playerId);

    BiConsumer<Integer, Runnable> getKeyBindingsRegisterer();

    Consumer<Runnable> getExitHookRegisterer();

    Consumer<Runnable> getLeaveHookRegisterer();

    void executeLeaveHooks();
}
