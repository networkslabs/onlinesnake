package ru.nsu.g.asidorov1.snakes.snake;

import ru.nsu.g.asidorov1.snakes.plane.BoundedPoint;

import java.util.function.Function;

public interface Snake extends Steerable, SnakeInfo {

    void zombify();

    void move(final Function<BoundedPoint, Boolean> isFood);
}
