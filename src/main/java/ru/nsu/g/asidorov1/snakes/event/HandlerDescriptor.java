package ru.nsu.g.asidorov1.snakes.event;

@FunctionalInterface
public interface HandlerDescriptor {

    void remove();
}
