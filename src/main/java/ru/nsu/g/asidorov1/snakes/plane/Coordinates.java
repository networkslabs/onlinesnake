package ru.nsu.g.asidorov1.snakes.plane;

public interface Coordinates {

    int getX();

    int getY();
}
