package ru.nsu.g.asidorov1.snakes.gui.menu;

@FunctionalInterface
public interface RunningGamesView {

    void updateView();
}
