package ru.nsu.g.asidorov1.snakes.descriptors.config;

public class InvalidConfigException extends Exception {

    public InvalidConfigException(final String message) {
        super(message);
    }
}
