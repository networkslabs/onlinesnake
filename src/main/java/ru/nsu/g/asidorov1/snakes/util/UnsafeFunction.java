package ru.nsu.g.asidorov1.snakes.util;

@FunctionalInterface
public interface UnsafeFunction<ArgumentT, ResultT> {

    ResultT apply(final ArgumentT argument) throws Exception;
}
