package ru.nsu.g.asidorov1.snakes.plane;

import lombok.ToString;

@ToString
public class UnboundedPoint {
    public int x;
    public int y;

    public Coordinates toCoordinates() {
        return new UnboundedFixedPoint(this.x, this.y);
    }
}
