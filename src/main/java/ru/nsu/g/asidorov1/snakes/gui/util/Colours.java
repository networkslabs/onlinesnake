package ru.nsu.g.asidorov1.snakes.gui.util;

import lombok.experimental.UtilityClass;

import java.awt.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.ThreadLocalRandom;

@UtilityClass
public class Colours {

    public final Color BACKGROUND_COLOUR = new Color(255, 255, 255);
    public final Color FOREGROUND_COLOUR = new Color(212, 212, 212);

    public final Color NAME_COLOUR = FOREGROUND_COLOUR;
    public final Color DEAD_SNAKE_COLOUR = FOREGROUND_COLOUR;
    public final Color RED = new Color(199, 84, 80);
    public final Color GREEN = new Color(73, 156, 84);
    public final Color LIGHT_GRAY = new Color(135, 147, 154);
    public final Color BLUE = new Color(53, 146, 196);
    public final Color YELLOW = new Color(244, 175, 61);

    public final Color INTERFACE_BACKGROUND = new Color(255, 255, 255);
    public final Color GAME_PANEL_BACKGROUND = new Color(255, 255, 255);
    public final Color LIGHT_LINING = new Color(100, 100, 100);
    public final Color LINING = new Color(81, 81, 81);
    public final Color DARK_LINING = new Color(49, 51, 53);
    public final Color TEXT = new Color(2, 0, 0);
    public final Color SCROLL_THUMB = new Color(255, 255, 255);
    public final Color TEXT_ENTRY_FORM = new Color(255, 255, 255);
    public final Color TOOLTIP = new Color(255, 255, 255);

    private final Collection<Color> snakeColours = new HashSet<>();

    static {
        snakeColours.add(LIGHT_GRAY);
        snakeColours.add(BLUE);
        snakeColours.add(YELLOW);
        snakeColours.add(Color.CYAN);
        snakeColours.add(Color.MAGENTA);
    }

    public Color getRandomColour() {
        var num = ThreadLocalRandom.current().nextInt(0, snakeColours.size());
        for (final var t : snakeColours) {
            --num;
            if (num < 0) {
                return t;
            }
        }
        throw new AssertionError();
    }
}
