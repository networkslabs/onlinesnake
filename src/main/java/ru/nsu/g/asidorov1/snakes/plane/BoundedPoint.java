package ru.nsu.g.asidorov1.snakes.plane;

public interface BoundedPoint extends Coordinates {

    Coordinates getBounds();

    BoundedPoint copy();
}
