package ru.nsu.g.asidorov1.snakes.event.events;

import lombok.RequiredArgsConstructor;
import ru.nsu.g.asidorov1.snakes.event.Event;

import java.net.InetSocketAddress;

@RequiredArgsConstructor
public class NodeTimedOut implements Event {

    public final InetSocketAddress nodeAddress;
}
