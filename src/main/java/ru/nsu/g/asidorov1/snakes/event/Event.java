package ru.nsu.g.asidorov1.snakes.event;

public interface Event {

    @SuppressWarnings("unchecked") // i sacrifice
    default <T extends Event> T get() throws ClassCastException {
        return (T) this;
    }
}
