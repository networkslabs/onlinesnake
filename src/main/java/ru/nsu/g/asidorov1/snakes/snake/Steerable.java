package ru.nsu.g.asidorov1.snakes.snake;

import ru.nsu.g.asidorov1.snakes.plane.Direction;

@FunctionalInterface
public interface Steerable {

    void changeDirection(final Direction direction);
}
