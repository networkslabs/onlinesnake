package ru.nsu.g.asidorov1.snakes.snake.impl;

import ru.nsu.g.asidorov1.snakes.plane.BoundedPoint;
import ru.nsu.g.asidorov1.snakes.plane.Direction;
import ru.nsu.g.asidorov1.snakes.snake.Snake;

@FunctionalInterface
public interface SnakeImplementationSupplier<SnakeType extends Snake> {

    SnakeType get(
            final int id,
            final BoundedPoint head,
            final Direction direction,
            final int size);
}
