package ru.nsu.g.asidorov1.snakes.descriptors.game;

import lombok.Getter;
import lombok.ToString;
import ru.nsu.g.asidorov1.snakes.descriptors.config.Config;
import ru.nsu.g.asidorov1.snakes.descriptors.player.PlayerInfo;
import ru.nsu.g.asidorov1.snakes.descriptors.snake.SnakeState;
import ru.nsu.g.asidorov1.snakes.plane.Coordinates;

@Getter
@ToString
public class GameDescriptor implements GameState {
    private int stateOrder;
    private final Iterable<SnakeState> snakes;
    private final Iterable<Coordinates> food;
    private final Iterable<PlayerInfo> players;
    private final Config config;

    public GameDescriptor(
            final int stateOrder,
            final Iterable<SnakeState> snakes,
            final Iterable<Coordinates> food,
            final Iterable<PlayerInfo> players,
            final Config config) {
        this.stateOrder = stateOrder;
        this.snakes = snakes;
        this.food = food;
        this.players = players;
        this.config = config;
    }

    public void incrementStateOrder() {
        this.stateOrder += 1;
    }

    public void setStateOrder(final int stateOrder) {
        this.stateOrder = stateOrder;
    }
}
