package ru.nsu.g.asidorov1.snakes.descriptors;

import lombok.experimental.UtilityClass;
import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.g.asidorov1.snakes.plane.Coordinates;

@UtilityClass
public class Utility {

    public SnakesProto.GameState.Coord coordinates(final Coordinates coordinates) {
        return SnakesProto.GameState.Coord.newBuilder()
                .setX(coordinates.getX())
                .setY(coordinates.getY())
                .build();
    }
}
