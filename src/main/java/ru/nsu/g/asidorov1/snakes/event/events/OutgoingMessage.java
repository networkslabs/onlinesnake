package ru.nsu.g.asidorov1.snakes.event.events;

import lombok.RequiredArgsConstructor;
import ru.nsu.g.asidorov1.snakes.event.Event;
import ru.nsu.g.asidorov1.snakes.message.AddressedMessage;

@RequiredArgsConstructor
public final class OutgoingMessage implements Event {

    public final AddressedMessage message;
}
