package ru.nsu.g.asidorov1.snakes.gui.menu;

import ru.nsu.g.asidorov1.snakes.descriptors.config.Config;
import ru.nsu.g.asidorov1.snakes.gui.game.SnakesGameView;

@FunctionalInterface
public interface GameStarter {

    void startGame(
            final String playerName,
            final Config gameConfig,
            final SnakesGameView gameView);
}
