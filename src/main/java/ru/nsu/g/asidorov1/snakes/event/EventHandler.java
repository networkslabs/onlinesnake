package ru.nsu.g.asidorov1.snakes.event;

@FunctionalInterface
public interface EventHandler {

    void handle(final Event message) throws Exception;
}
