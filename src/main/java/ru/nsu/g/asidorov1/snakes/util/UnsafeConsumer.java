package ru.nsu.g.asidorov1.snakes.util;

@FunctionalInterface
public interface UnsafeConsumer<T> {

    void accept(final T value) throws Exception;
}
