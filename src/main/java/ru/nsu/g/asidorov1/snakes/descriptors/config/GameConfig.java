package ru.nsu.g.asidorov1.snakes.descriptors.config;

public interface GameConfig {

    int getPlaneWidth();

    int getPlaneHeight();

    int getFoodStatic();

    float getFoodPerPlayer();

    int getStateDelayMs();

    float getFoodSpawnOnDeathChance();
}
