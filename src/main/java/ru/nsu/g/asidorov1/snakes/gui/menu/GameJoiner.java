package ru.nsu.g.asidorov1.snakes.gui.menu;

import ru.nsu.g.asidorov1.snakes.descriptors.config.Config;
import ru.nsu.g.asidorov1.snakes.gui.game.SnakesGameView;

import java.net.InetSocketAddress;
import java.util.function.Consumer;

@FunctionalInterface
public interface GameJoiner {

    void joinGame(
            final String playerName,
            final Config gameConfig,
            final InetSocketAddress hostAddress,
            final SnakesGameView gameView,
            final Runnable onSuccess,
            final Consumer<String> onError);
}
