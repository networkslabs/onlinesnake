package ru.nsu.g.asidorov1.snakes.message;

import lombok.experimental.UtilityClass;
import lombok.val;
import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.g.asidorov1.snakes.descriptors.game.GameState;
import ru.nsu.g.asidorov1.snakes.plane.Direction;

import java.util.concurrent.atomic.AtomicLong;

@UtilityClass
public class MessageFactory {

    private final AtomicLong sequenceNumber = new AtomicLong(0);

    private SnakesProto.GameMessage.Builder createBuilderWithSequenceNumber() {
        return SnakesProto.GameMessage.newBuilder()
                .setMsgSeq(sequenceNumber.getAndIncrement());
    }

    public SnakesProto.GameMessage createPingMessage() {
        return createBuilderWithSequenceNumber()
                .setPing(
                        SnakesProto.GameMessage.PingMsg.newBuilder()
                                .build())
                .build();
    }

    public SnakesProto.GameMessage createSteerMessage(final Direction direction) {
        return createBuilderWithSequenceNumber()
                .setSteer(
                        SnakesProto.GameMessage.SteerMsg.newBuilder()
                                .setDirection(SnakesProto.Direction.valueOf(direction.toString()))
                                .build())
                .build();
    }

    public SnakesProto.GameMessage createAcknowledgementMessage(
            final long sequenceNumber,
            final int senderId,
            final int receiverId) {
        return SnakesProto.GameMessage.newBuilder()
                .setMsgSeq(sequenceNumber)
                .setAck(
                        SnakesProto.GameMessage.AckMsg.newBuilder()
                                .build())
                .setSenderId(senderId)
                .setReceiverId(receiverId)
                .build();
    }

    public SnakesProto.GameMessage createStateMessage(final GameState state) {
        return createBuilderWithSequenceNumber()
                .setState(
                        SnakesProto.GameMessage.StateMsg.newBuilder()
                                .setState(state.toMessage())
                                .build())
                .build();
    }

    public SnakesProto.GameMessage createAnnouncementMessage(
            final GameState state) {
        val playersBuilder = SnakesProto.GamePlayers.newBuilder();
        state.getPlayers().forEach(it -> playersBuilder.addPlayers(it.toMessage()));

        return createBuilderWithSequenceNumber()
                .setAnnouncement(
                        SnakesProto.GameMessage.AnnouncementMsg.newBuilder()
                                .setConfig(state.getConfig().toMessage())
                                .setPlayers(playersBuilder.build()))
                .build();

    }

    public SnakesProto.GameMessage createJoinMessage(
            final String name,
            final boolean isBot,
            final boolean watchOnly) {
        return createBuilderWithSequenceNumber()
                .setJoin(
                        SnakesProto.GameMessage.JoinMsg.newBuilder()
                                .setName(name)
                                .setOnlyView(watchOnly)
                                .setPlayerType(isBot ? SnakesProto.PlayerType.ROBOT : SnakesProto.PlayerType.HUMAN)
                                .build())
                .build();
    }

    public SnakesProto.GameMessage createErrorMessage(final String what) {
        return createBuilderWithSequenceNumber()
                .setError(
                        SnakesProto.GameMessage.ErrorMsg.newBuilder()
                                .setErrorMessage(what)
                                .build())
                .build();
    }

    public SnakesProto.GameMessage createRoleChangingMessage(
            final SnakesProto.NodeRole senderRole,
            final SnakesProto.NodeRole receiverRole,
            final int senderId,
            final int receiverId) {
        return createBuilderWithSequenceNumber()
                .setRoleChange(
                        SnakesProto.GameMessage.RoleChangeMsg.newBuilder()
                                .setSenderRole(senderRole)
                                .setReceiverRole(receiverRole)
                                .build())
                .setSenderId(senderId)
                .setReceiverId(receiverId)
                .build();
    }

    public SnakesProto.GameMessage createRoleChangingMessage(
            final SnakesProto.NodeRole senderRole,
            final int senderId,
            final int receiverId) {
        return createBuilderWithSequenceNumber()
                .setRoleChange(
                        SnakesProto.GameMessage.RoleChangeMsg.newBuilder()
                                .setSenderRole(senderRole)
                                .build())
                .setSenderId(senderId)
                .setReceiverId(receiverId)
                .build();
    }
}
