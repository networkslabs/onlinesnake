package ru.nsu.g.asidorov1.snakes.net;

import lombok.experimental.UtilityClass;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;

@UtilityClass
public class Utility {

    public boolean isThisMyIpAddress(final InetAddress address) {
        // https://stackoverflow.com/a/2406819
        // Check if the address is a valid special local or loop back
        if (address.isAnyLocalAddress() || address.isLoopbackAddress())
            return true;

        // Check if the address is defined on any interface
        try {
            return NetworkInterface.getByInetAddress(address) != null;
        } catch (final SocketException e) {
            return false;
        }
    }
}
