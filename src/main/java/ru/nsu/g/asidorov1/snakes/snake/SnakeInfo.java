package ru.nsu.g.asidorov1.snakes.snake;

import ru.nsu.g.asidorov1.snakes.plane.BoundedPoint;
import ru.nsu.g.asidorov1.snakes.plane.Direction;

import java.util.function.Consumer;

public interface SnakeInfo {


    BoundedPoint getHead();

    void forEachSegment(final Consumer<BoundedPoint> action);

    int getPlayerId();


    Direction getDirection();

    boolean isZombie();
}
