package ru.nsu.g.asidorov1.snakes.gui.game;

import lombok.val;
import ru.nsu.g.asidorov1.snakes.game.GameModel;
import ru.nsu.g.asidorov1.snakes.gui.util.Colours;
import ru.nsu.g.asidorov1.snakes.gui.util.GuiUtils;

import javax.swing.*;
import java.awt.*;

final class GamePanel extends JPanel {

    final SnakesPanel snakesPanel;
    final SidePanel sidePanel;

    GamePanel(
            final GameWindow view,
            final GameModel gameState) {
        super(new BorderLayout());

        this.snakesPanel = new SnakesPanel(view, gameState);
        this.sidePanel = new SidePanel(view, gameState, this.snakesPanel);

        this.add(this.snakesPanel, BorderLayout.WEST);
        val sep = new JSeparator(SwingConstants.VERTICAL);
        GuiUtils.setColours(sep, Colours.LINING, Colours.BACKGROUND_COLOUR);
        this.add(sep, BorderLayout.CENTER);
        this.add(this.sidePanel, BorderLayout.EAST);
    }

    void update() {
        this.snakesPanel.repaint();
        this.sidePanel.scorePanel.updateScores();
    }
}
