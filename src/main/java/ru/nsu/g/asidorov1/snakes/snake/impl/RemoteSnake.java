package ru.nsu.g.asidorov1.snakes.snake.impl;

import lombok.val;
import ru.nsu.g.asidorov1.snakes.descriptors.snake.SnakeState;
import ru.nsu.g.asidorov1.snakes.plane.BoundedMovablePoint;
import ru.nsu.g.asidorov1.snakes.plane.BoundedPoint;
import ru.nsu.g.asidorov1.snakes.plane.Coordinates;
import ru.nsu.g.asidorov1.snakes.plane.Direction;
import ru.nsu.g.asidorov1.snakes.snake.SnakeInfo;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.function.Consumer;

public class RemoteSnake implements SnakeInfo {

    private final
    Coordinates bounds;
    private final Deque<Coordinates> keyPoints = new ArrayDeque<>();

    private
    Direction direction;
    private boolean alive = true;
    private final int playerId;

    public RemoteSnake(
            final int playerId,
            final Coordinates bounds,
            final SnakeState descriptor) {
        this.playerId = playerId;
        this.bounds = bounds;
        this.setKeyPoints(descriptor.getPoints());
        this.setAlive(descriptor.isAlive());
        this.setDirection(descriptor.getDirection());
    }

    private void setKeyPoints(final List<Coordinates> keyPoints) {
        this.keyPoints.clear();
        this.keyPoints.addAll(keyPoints);
    }

    private void setAlive(final boolean alive) {
        this.alive = alive;
    }

    private void setDirection(final Direction direction) {
        this.direction = direction;
    }

    @Override
    public BoundedPoint getHead() {
        if (this.keyPoints.isEmpty()) {
            throw new IllegalStateException("Headless");
        }
        return new BoundedMovablePoint(this.keyPoints.peekFirst(), this.bounds);
    }

    @Override
    public Direction getDirection() {
        if (this.direction == null) {
            throw new IllegalStateException("Lost it's way"); // i'm probably gonna hate myself for these error messages
        }
        return this.direction;
    }

    @Override
    public void forEachSegment(
            final Consumer<BoundedPoint> action) {
        if (this.keyPoints.isEmpty()) {
            return;
        }

        val head = this.keyPoints.peekFirst();
        val point = new BoundedMovablePoint(head, this.bounds);
        action.accept(point);
        this.keyPoints.stream()
                .skip(1)
                .forEach(offset -> {
                    point.forEachWithinExceptSelf(offset, action);
                    point.move(offset);
                });
    }

    @Override
    public int getPlayerId() {
        return this.playerId;
    }

    @Override
    public boolean isZombie() {
        return !this.alive;
    }
}
