package ru.nsu.g.asidorov1.snakes.util;

import java.util.Timer;
import java.util.logging.Logger;

public class TimerWithLogger extends Timer {

    private static final Logger logger = Logger.getLogger(TimerWithLogger.class.getSimpleName());

    public TimerWithLogger() {
        super();
    }

    public TimerWithLogger(final boolean isDaemon) {
        super(isDaemon);
    }

    @Override
    public void cancel() {
        logger.info(Thread.currentThread().getName() + " cancelled the timer");
        super.cancel();
    }
}
