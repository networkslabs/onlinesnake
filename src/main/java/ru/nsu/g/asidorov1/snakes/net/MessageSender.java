package ru.nsu.g.asidorov1.snakes.net;

import lombok.RequiredArgsConstructor;
import lombok.val;
import ru.nsu.g.asidorov1.snakes.message.AddressedMessage;
import ru.nsu.g.asidorov1.snakes.util.Constants;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.function.Supplier;
import java.util.logging.Logger;

@RequiredArgsConstructor
public class MessageSender {

    private static final Logger logger = Logger.getLogger(MessageSender.class.getSimpleName());

    private final DatagramSocket out;
    private final MessageHistory messageHistory;

    private Supplier<InetSocketAddress> masterAddressSupplier;

    public void send(final AddressedMessage message) throws IOException {
        var address = this.getActualDestinationAddress(message);
        if (address == null) {
            return;
        }
        if (this.out.getLocalPort() == address.getPort() && Utility.isThisMyIpAddress(address.getAddress())) {
            logger.info("Message addressed to self, won't send");
            return;
        }
        val contents = message.getMessage();
        val serializedSize = contents.getSerializedSize();
        if (serializedSize > Constants.MAX_PACKET_SIZE_B) {
            logger.warning(" Message serialized size is too big (" + serializedSize
                    + " > " + Constants.MAX_PACKET_SIZE_B + "), dropped");
            return;
        }

        val bytes = contents.toByteArray();
        val packet = new DatagramPacket(bytes, 0, bytes.length, address);

        if (contents.hasPing() && !this.messageHistory.isConnectedTo(address)) {
            logger.info("Destination address " + address + " is unknown for a ping message");
            return;
        }
        if (contents.hasJoin()) {
            logger.info("Sent a join request to " + address);
        }

        this.out.send(packet);
        message.decrementRetriesCount();
        logger.finest("Sent " + serializedSize + " bytes long packet to " + address);

        if (Constants.announceAddress.equals(address) || contents.hasAck()) {
            // oh yes, fire & forget, my favourite type of messaging
            return;
        }

        this.messageHistory.messageSent(address, message);
    }

    private synchronized InetSocketAddress getActualDestinationAddress(
            final AddressedMessage message) {
        if (!message.isAddressedToMaster()) {
            return message.getAddress();
        }
        if (this.masterAddressSupplier == null) {
            logger.warning("Message is for master but no master address supplier provided");
            return null;
        }
        val address = this.masterAddressSupplier.get();
        if (address == null) {
            logger.warning("Master address supplier returned null: is this node the master?");
            return null;
        }
        return address;
    }

    public synchronized void setMasterAddressSupplier(
            final Supplier<InetSocketAddress> masterAddressSupplier) {
        this.masterAddressSupplier = masterAddressSupplier;
    }
}
