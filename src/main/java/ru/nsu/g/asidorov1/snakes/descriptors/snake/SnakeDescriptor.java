package ru.nsu.g.asidorov1.snakes.descriptors.snake;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.nsu.g.asidorov1.snakes.plane.Coordinates;
import ru.nsu.g.asidorov1.snakes.plane.Direction;

import java.util.List;

@RequiredArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
public class SnakeDescriptor implements SnakeState {
    @EqualsAndHashCode.Include
    private final int playerId;
    private final List<Coordinates> points;
    private final boolean alive;
    private final
    Direction direction;
}
