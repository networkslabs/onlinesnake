package ru.nsu.g.asidorov1.snakes.descriptors.game;

import lombok.val;
import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.g.asidorov1.snakes.descriptors.Utility;
import ru.nsu.g.asidorov1.snakes.descriptors.config.Config;
import ru.nsu.g.asidorov1.snakes.descriptors.player.PlayerInfo;
import ru.nsu.g.asidorov1.snakes.descriptors.snake.SnakeState;
import ru.nsu.g.asidorov1.snakes.plane.Coordinates;
import ru.nsu.g.asidorov1.snakes.plane.UnboundedFixedPoint;

import java.util.ArrayList;

public interface GameState {

    int getStateOrder();

    Iterable<SnakeState> getSnakes();

    Iterable<Coordinates> getFood();

    Iterable<PlayerInfo> getPlayers();

    Config getConfig();

    static GameState fromMessage(final SnakesProto.GameStateOrBuilder state) {
        val config = state.getConfig();
        val playersList = state.getPlayers();
        val stateOrder = state.getStateOrder();

        val snakes = new ArrayList<SnakeState>();
        for (int i = 0; i < state.getSnakesCount(); i += 1) {
            snakes.add(SnakeState.fromMessage(state.getSnakes(i)));
        }
        val food = new ArrayList<Coordinates>();
        for (int i = 0; i < state.getFoodsCount(); i += 1) {
            val point = state.getFoods(i);
            food.add(new UnboundedFixedPoint(point.getX(), point.getY()));
        }
        val players = new ArrayList<PlayerInfo>();
        for (int i = 0; i < playersList.getPlayersCount(); i += 1) {
            val player = playersList.getPlayers(i);
            players.add(PlayerInfo.fromMessage(player));
        }

        return new GameDescriptor(
                stateOrder,
                snakes,
                food,
                players,
                Config.fromMessage(config));
    }

    default SnakesProto.GameState toMessage() {
        val builder = SnakesProto.GameState.newBuilder()
                .setStateOrder(this.getStateOrder())
                .setConfig(this.getConfig().toMessage());
        val players = SnakesProto.GamePlayers.newBuilder();
        this.getPlayers().forEach(it -> players.addPlayers(it.toMessage()));
        builder.setPlayers(players);
        this.getSnakes().forEach(it -> builder.addSnakes(it.toMessage()));
        this.getFood().forEach(it -> builder.addFoods(Utility.coordinates(it)));
        return builder.build();
    }
}
