package ru.nsu.g.asidorov1.snakes.net;

import lombok.RequiredArgsConstructor;
import lombok.val;
import ru.nsu.g.asidorov1.snakes.event.EventProcessor;
import ru.nsu.g.asidorov1.snakes.event.events.AnnouncementTimedOut;
import ru.nsu.g.asidorov1.snakes.event.events.NodeTimedOut;
import ru.nsu.g.asidorov1.snakes.event.events.NotAcknowledged;
import ru.nsu.g.asidorov1.snakes.event.events.TimeToPing;

import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.logging.Logger;

@RequiredArgsConstructor
public final class TimeoutWatcher {

    private static final Logger logger = Logger.getLogger(TimeoutWatcher.class.getSimpleName());

    private final EventProcessor out;
    private final MessageHistory messageHistory;

    private final int ackTimeout;
    private final int pingTimeout;
    private final int nodeTimeout;
    private final int announcementTimeout;

    public void handleTimeouts() throws Exception {
        synchronized (this.messageHistory) {
            val toRemove = new HashSet<InetSocketAddress>();

            logger.finest("Checking last received time");
            this.messageHistory.forEachDisconnected(this.nodeTimeout, address -> {
                logger.fine("Haven't received anything from " + address + " for longer than timeout");
                toRemove.add(address);
                this.out.submit(new NodeTimedOut(address));
            });

            toRemove.forEach(this.messageHistory::removeConnectionRecord);
            toRemove.clear();

            logger.finest("Checking sent messages");
            this.messageHistory.forEachNotAcknowledged(this.ackTimeout, message -> {
                logger.finest(
                        "Message " + message.getMessage().getMsgSeq() + " addressed to "
                                + (message.isAddressedToMaster() ? "master" : message.getAddress())
                                + " not acknowledged within timeout");
                val toAddress = message.isAddressedToMaster()
                        ? this.messageHistory.getRealDestinationAddress(message)
                        : message.getAddress();
                if (toAddress == null) {
                    logger.warning("No real destination address for message " + message.getMessage().getMsgSeq());
                } else {
                    // For special cases when this node started sending messages to another node
                    // without knowing anything about it (i.e. if it's even online)
                    if (!message.retriesLeft() && !this.messageHistory.isConnectedTo(toAddress)) {
                        // If a message has no retries left and no messages were ever received from it's destination
                        // generate a NodeTimedOut event to tell whoever who's been sending the message to stop
                        // sending anything to this destination
                        this.out.submit(new NodeTimedOut(toAddress));
                        toRemove.add(toAddress);
                        logger.info("Submitted NodeTimedOut event");
                    }
                }
                if (message.retriesLeft()) {
                    this.out.submit(new NotAcknowledged(message));
                }
            });

            toRemove.forEach(this.messageHistory::removeConnectionRecord);

            logger.finest("Checking last contacted to time");
            this.messageHistory.forEachNotContacted(this.pingTimeout, address -> {
                logger.fine("Haven't sent anything to " + address + " for longer than timeout");
                this.out.submit(new TimeToPing(address));
            });

            logger.finest("Checking announcements receive time");
            this.messageHistory.forEachOldAnnouncement(this.announcementTimeout, address -> {
                logger.finest("Announcement form " + address + " timed out");
                this.out.submit(new AnnouncementTimedOut(address));
            });
        }
    }
}
